<?php

abstract class ColorLayout {
    private $words;

    abstract function getStylesColor();

    public function applyStyle() {
        $styleObj = $this->getStylesColor();
        $styleObj->message();
    }
}

class WrongStyles extends ColorLayout {
    private $words;

    public function __construct($words)
    {
        $this->words = $words;
    }

    public function getStylesColor()
    {
        return new WrongStylesConnector($this->words);
    }
}

class CalmStyles extends ColorLayout {
    private $words;

    public function __construct($words)
    {
        $this->words = $words;
    }

    public function getStylesColor()
    {
        return new CalmStylesConnector($this->words);
    }
}

interface ColorLayoutConnector {
    public function message();
}

class WrongStylesConnector implements ColorLayoutConnector {

    private $words;

    public function __construct($words)
    {
        $this->words = $words;
    }

    public function message()
    {
        echo "<h3 style='color: red;'>{$this->words}</h3>";
    }
}


class CalmStylesConnector implements ColorLayoutConnector {

    private $words;

    public function __construct($words)
    {
        $this->words = $words;
    }

    public function message()
    {
        echo "<h3 style='color: green; font-style: italic;'>{$this->words}</h3>";
    }
}

$styleMessage = new WrongStyles('Hello wrong World');
$styleMessage->applyStyle();

$styleMessage = new CalmStyles('Hello calm World');
$styleMessage->applyStyle();